package cs425.swe.rentacar.controller;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Home Controller @author Dios
 */

@Controller
@Api(tags = "HOME")
public class HomeController {
    /**
     * Mapping
     **/

    @GetMapping(value = {"/", "/index"})
    public String index() {
        return "index";
    }

    @GetMapping(value = "/**/{path:[^.]*}")
    public String any() {
        return "forward:/";
    }

}
