const PROXY_CONFIG = [
  {
    context: [
      "/api/test/**",
      "/api/auth/**",
      "/auth/**",
      "/user/**",
      "/car/**",
      "/upload/**",
      "/rental/**"
      /* ... */
    ],
    target: "http://localhost:8080",
    secure: false,
    logLevel: 'debug'
  }
];

module.exports = PROXY_CONFIG;

