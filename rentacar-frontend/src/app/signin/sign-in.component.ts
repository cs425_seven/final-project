import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LogService} from '../service/log-service';
import {AuthService} from '../service/auth.service';
import {NotifierService} from '../service/notify.service';

@Component({
  selector: 'app-login',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  hero = {
    name: 'Eugene'
  };

  user = {
    // userName: 'locnv',
    // password: '1234565678'

    userName: '',
    password: '1234565678'
  };

  constructor(
    private router: Router,
    private logger: LogService,
    private notifier: NotifierService,
    private authService: AuthService) {}

  ngOnInit() { }

  doLogin(username: string, pwd: string) {
    if (!username || !pwd) {
      this.notifier.error('Username or password is empty.');
      return;
    }

    this.authService.signin(username, pwd)
      .then((isOk: boolean) => {
        this.logger.info('Login responded! -> ' + isOk);

        let targetPage = '/dashboard';
        const loggedInUser = this.authService.getLoggedInUser();
        if (this.authService.isAdmin(loggedInUser)) {
          // Default page for admin
          targetPage = '/user';
        }

        this.router.navigate([ targetPage ]).then();
      })
      .catch((err) => {
        this.logger.error('Failed to logged!');
        this.notifier.error('Login failed. Please check your username and / or password and tray again.');
      });
  }

  onBtnLoginClicked(username: string, pwd: string) {
    this.doLogin(username, pwd);
  }

  onKeyUpPressed(e) {
    if (e.key !== 'Enter') {
      return;
    }

    this.doLogin(this.user.userName, this.user.password);

  }
}
