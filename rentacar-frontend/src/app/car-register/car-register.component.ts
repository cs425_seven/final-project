import {Component, OnInit} from '@angular/core';
import {LogService} from '../service/log-service';
import {NotifierService} from '../service/notify.service';
import {Router} from '@angular/router';
import {Car} from '../model/car';
import {CarService} from '../service/car-service';
import {HttpHeaders, HttpParams} from '@angular/common/http';
import {Images} from '../model/images';

@Component({
  selector: 'app-car-register',
  templateUrl: './car-register.component.html',
  styleUrls: ['./car-register.component.scss']
})
export class CarRegisterComponent implements OnInit {

  car: Car;
  formData: FormData;
  images: any[];

  constructor(
    private logger: LogService,
    private notifier: NotifierService,
    private carService: CarService,
    private router: Router) {
    this.formData =  new FormData();

    this.car = new Car();
  }

  onChange(event: any) {

    const files = [].slice.call(event.event.target.files);

    // const headers = new HttpHeaders();
    // this is the important step. You need to set content type as null
    // headers.set('Content-Type', null);
    // headers.set('Accept', 'multipart/form-data');
    // const params = new HttpParams();

    this.images = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < files.length; i++) {
      this.formData.append('files', files[i], files[i].name);

      this.readURL(files[i]);
    }

    const response = this.carService.uploadImages(this.formData);
    response.then((rs: any) => {
      this.logger.info(rs);
    })
    .catch(this.logger.error);

    this.car.images = files.map(f => new Images(f.name));
  }

  readURL(file) {

    const reader = new FileReader();

    reader.onload = (e: any) => {
      this.images.push({
        src: e.target.result,
        alt: file.name
      });
    };

    reader.readAsDataURL(file);
  }

  ngOnInit() {
    this.car = new Car();

  }

  onClickSubmitRegister() {
    this.car.available = true;
    this.carService.registerCar(this.car)
      .then((resp) => {
      this.logger.info('Car register responded -> ' + JSON.stringify(resp));
      this.notifier.success('Registered new car.');
      this.router.navigate([ '/car' ]).then();
    })
      .catch((err) => {
        this.logger.error('Failed to register carr -> ' + err);
        if (err instanceof Error) {
          this.notifier.error(err);
        } else {
          this.notifier.error('Failed to register car. Unknown error.');
        }
      });
  }
}
