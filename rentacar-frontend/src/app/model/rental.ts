import { User } from './user';
import { Car } from './car';

export class Rental {
  id: number;
  car: Car;
  user: User;
  status: string;
  price: number;
  createDate: Date;
  pickUpDate: Date;
  expectedDate: Date;
  returnDate: Date;
  rejectDesc: string;
}
