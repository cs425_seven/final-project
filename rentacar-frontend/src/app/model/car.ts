import {User} from './user';
import {Images} from './images';

export class Car {
  id: number;
  carBrand: string;
  model: string;
  year: number;
  price: number;
  seats: number;
  owner: User;
  description: string;
  status: string;
  pickUpLocation: string;
  images: Images[];
  available: boolean;

  constructor() {
    this.images = [];
  }

}
