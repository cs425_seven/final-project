export class ResponseDTO {
  header: HeaderDTO;
  body: BodyDTO;
}

export class HeaderDTO {
  code: number;
  status: string;
}

export class BodyDTO {
  response: object;
  error: object;
  success: object;
}

