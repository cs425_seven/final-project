import {first} from 'rxjs/operators';

export class User {

  constructor() {
    this.roles = [];
  }

  id: number;
  userName: string;
  password: string;
  passwordConfirm: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  status: string;
  createDate: Date;
  roles: string[];

  setData(userName: string, firstName: string, lastName: string, email: string, phone: string, status: string): User {
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.status = status;

    return this;
  }
}
