export class Roles {
  static Admin = 'ADMIN';
  static CarOwner = 'OWNER';
  static CarRenter = 'RENTER';
}

export class RentalStatus {
  static Pending = 'PENDING';
  static Approved = 'APPROVED';
  static Declined = 'DECLINED';
  static Returned = 'RETURNED';
}
