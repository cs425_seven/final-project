export const RENTALS: any = [
  {
    id: 101,
    car: {
      carBrand: 'Toyota',
      model: 'M1'
    },
    user: {
      userName: 'rentar-01'
    },
    status: 'Pending',
    price: 120.0,
    createdDate: new Date()
  },
  {
    id: 102,
    car: {
      carBrand: 'Car #xxx'
    },
    user: {
      userName: 'User name'
    },
    status: 'Pending',
    price: 120.0,
    createdDate: new Date()
  },
];
