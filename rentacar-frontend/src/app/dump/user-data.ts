
export const USERS: any = [
  { userName: 'test01', firstName: 'test', lastName: '001', email: 'test01@gmail.com', phone: '1122121122', status: 'Active' },
  { userName: 'test02', firstName: 'test', lastName: '002', email: 'test02@gmail.com', phone: '1122121122', status: 'Active' },
  { userName: 'test03', firstName: 'test', lastName: '003', email: 'test03@gmail.com', phone: '1122121122', status: 'Active' },
  { userName: 'test04', firstName: 'test', lastName: '004', email: 'test04@gmail.com', phone: '1122121122', status: 'Blocked' },
  { userName: 'test05', firstName: 'test', lastName: '005', email: 'test05@gmail.com', phone: '1122121122', status: 'Blocked' },
  { userName: 'test06', firstName: 'test', lastName: '006', email: 'test06@gmail.com', phone: '1122121122', status: 'Active' },
  { userName: 'test07', firstName: 'test', lastName: '007', email: 'test07@gmail.com', phone: '1122121122', status: 'Active' },
  { userName: 'test08', firstName: 'test', lastName: '008', email: 'test08@gmail.com', phone: '1122121122', status: 'Blocked' },
  { userName: 'test09', firstName: 'test', lastName: '009', email: 'test09@gmail.com', phone: '1122121122', status: 'Active' },
  { userName: 'test10', firstName: 'test', lastName: '010', email: 'test10@gmail.com', phone: '1122121122', status: 'Active' },
];
