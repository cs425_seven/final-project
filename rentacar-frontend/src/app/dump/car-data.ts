
export const CARS: any = [
  {
    id: 102,
    carBrand: 'Chevrolet IMPALA LT',
    model: 'Model #1',
    year: 1998,
    price: 25,
    seats: 7,
    user: {},
    description: '2006 Chevrolet Impala LT This item will sell at auction on' ,
    images: [ '/assets/image/car-6.jpeg', '/assets/image/car-5.jpeg' ],
    status: 'Very Good',
    isAvailable: true
  },
  {
    id: 103,
    carBrand: 'Chevrolet IMPALA LD',
    model: 'Model #2',
    year: 1998,
    price: 32,
    seats: 7,
    user: {},
    description: '2006 Chevrolet Impala LT This item will sell at auction on October 30, 2019 It will sell ' +
      'regardless of price. Send Message for link to more photos, item and auction details, check latest bid ' +
      'and to place your bid. Or visit Purple Wave site and search item #DH6168',
    images: [ '/assets/image/car-6.jpeg', '/assets/image/car-5.jpeg' ],
    status: 'Very Good',
    isAvailable: true
  },
  {
    id: 104,
    carBrand: 'Chevrolet IMPALA LD123',
    model: 'Model #3',
    year: 1998,
    price: 32,
    seats: 7,
    user: {},
    description: '2006 Chevrolet Impala LT This item will sell at auction on October 30, 2019 It will sell ' +
      'regardless of price. Send Message for link to more photos, item and auction details, check latest bid ' +
      'and to place your bid. Or visit Purple Wave site and search item #DH6168',
    images: [ '/assets/image/car-6.jpeg', '/assets/image/car-5.jpeg' ],
    status: 'Good',
    isAvailable: true
  },
];
