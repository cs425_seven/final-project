import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './signin/sign-in.component';
import { SignUpComponent } from './signup/sign-up.component';
import { UserComponent } from './admin-user/user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CarRegisterComponent } from './car-register/car-register.component';
import { RentalComponent } from './admin-rental/rental.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CarComponent } from './admin-car/car.component';
import { StatisticComponent } from './admin-statistic/statistic.component';
import { TestComponent } from './test/test.component';
import {CarDetailComponent} from './car-detail/car-detail.component';


const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'login', component: SignInComponent },
  { path: 'register', component: SignUpComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'car-detail', component: CarDetailComponent },

  { path: 'car-register', component: CarRegisterComponent },

  /* Admin area */
  { path: 'user', component: UserComponent },
  { path: 'car', component: CarComponent },
  { path: 'rental', component: RentalComponent },
  { path: 'user-add', component: SignUpComponent },
  { path: 'statistic', component: StatisticComponent },

  /* Test */
  { path: 'test', component: TestComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

  isAuthenticatedPage(path: string): boolean {
    let isAuthPage = true;
    switch (path) {
      case '':
      case 'login':
      case 'logout':
      case 'register':
      case 'test':
        isAuthPage = false;
        break;
    }

    return isAuthPage;
  }

}
