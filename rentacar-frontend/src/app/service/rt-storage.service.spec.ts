import { TestBed } from '@angular/core/testing';

import { RtStorageService } from './rt-storage.service';

describe('RtStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RtStorageService = TestBed.get(RtStorageService);
    expect(service).toBeTruthy();
  });
});
