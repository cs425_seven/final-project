import { Injectable } from '@angular/core';
import { HttpService } from './http-service';
import { LogService } from './log-service';
import { User } from '../model/user';
import { RtStorageService } from './rt-storage.service';
import { UtilService } from './util-service';
import { Roles } from '../model/constant';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private logger: LogService,
    private httpService: HttpService,
    private util: UtilService,
    private rtStorage: RtStorageService) {

  }

  static hasRole(user: User, role: string): boolean {
    if (!user || !user.roles || user.roles.length === 0) {
      return false;
    }

    return user.roles.includes(role);
  }

  signup(user: User) {
    const url = 'auth/signup';

    return this.httpService.sendPostRequest(url, user);
  }

  signin(username: string, password: string) {

    const url = 'auth/signin';
    const d = {
      username,
      password
    };

    const http = this.httpService;

    return new Promise((resolve, reject) => {
      http.sendPostRequest(url, d).then((respBody) => {
        console.log(respBody);
        // @ts-ignore
        // this.rtStorage.setData(this.rtStorage.TOKEN_KEY, respBody.response.token);
        this.util.setCookie(this.rtStorage.TOKEN_KEY, respBody.response.token, 1);
        // @ts-ignore
        // this.rtStorage.setData(this.rtStorage.LOGGEDIN_USER, respBody.response.user);
        this.util.setCookie(this.rtStorage.LOGGEDIN_USER, respBody.response.user, 1);
        resolve(true);
      })
      .catch((err) => reject(err));
    });

  }

  signOut() {
    // const url = 'auth/signout';
    //
    // const http = this.httpService;
    //
    // return new Promise((resolve, reject) => {
    //   http.sendPostRequest(url, {}).then((respBody) => {
    //     this.rtStorage.setData(this.rtStorage.LOGGEDIN_USER, null);
    //     this.rtStorage.setData(this.rtStorage.TOKEN_KEY, null);
    //
    //     resolve(true);
    //   })
    //   .catch((err) => reject(err));
    // });

    return new Promise((resolve, reject) => {

      // this.rtStorage.setData(this.rtStorage.LOGGEDIN_USER, null);
      // this.rtStorage.setData(this.rtStorage.TOKEN_KEY, null);
      this.util.eraseCookie(this.rtStorage.LOGGEDIN_USER);
      this.util.eraseCookie(this.rtStorage.TOKEN_KEY);
      setTimeout(resolve.bind(null, true), 100);
    });
  }

  getLoggedInUser(): User {
    // const loggedInUser = this.rtStorage.getData(this.rtStorage.LOGGEDIN_USER);
    const loggedInUserStr = this.util.getCookie(this.rtStorage.LOGGEDIN_USER);
    if (!loggedInUserStr || loggedInUserStr.length === 0) {
      return null;
    }

    const loggedInUser = JSON.parse(loggedInUserStr);

    return loggedInUser as User;
  }

  isAdmin(user: User): boolean {
    if (!user || !user.roles) {
      return false;
    }

    return AuthService.hasRole(user, Roles.Admin);
  }

  isCarOwner(user: User): boolean {

    if (!user || !user.roles) {
      return false;
    }

    return AuthService.hasRole(user, Roles.CarOwner);
  }

}
