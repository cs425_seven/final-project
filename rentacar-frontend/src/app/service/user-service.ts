import { Injectable } from '@angular/core';
import { HttpService } from './http-service';
import { USERS } from '../dump/user-data';
import { User } from '../model/user';


@Injectable()
export class UserService {

  constructor(private httpService: HttpService) {
  }

  getAllUsers() {
    //  const url = 'user/v1/list';
    const url = 'user/list';
    return this.httpService.sendGetRequest(url, {});

    // return Promise.resolve(USERS);
  }

  searchUsers(searchTerm: string) {
    const url = 'user/search';
    return this.httpService.sendGetRequest(url, {
      searchString: searchTerm
    });
  }

  getUsersPaging(pageno: number) {
    // const url = `user/v1/list`;
    const url = `user/list`;
    return this.httpService.sendGetRequest(url, {
      pageno
    });

    // return Promise.resolve(USERS);
  }

  getUserById(id: number) {
    const url = `user/${id}`;
    return this.httpService.sendGetRequest(url, {});
  }

  blockUser(user: User) {
    // TODO replace http command
    const url = 'user/updateStatus';
    user.status = 'Blocked';
    return this.httpService.sendPostRequest(url, user);

    // return Promise.resolve(true);
  }

  unBlockUser(user: User) {
    const url = 'user/updateStatus';
    user.status = 'Active';
    return this.httpService.sendPostRequest(url, user);

    // return Promise.resolve(true);
  }

}
