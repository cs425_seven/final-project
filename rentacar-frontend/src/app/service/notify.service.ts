import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotifierService {

  constructor(private toastr: ToastrService) { }

  success(msg: any)   { this.toastr.success(msg); }
  error(msg: any)  { this.toastr.error(msg); }
}
