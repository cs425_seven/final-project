import { Injectable } from '@angular/core';
import { HttpService } from './http-service';
import { Rental } from '../model/rental';
import { Car } from '../model/car';
import { AuthService } from './auth.service';

@Injectable()
export class RentalService {

  constructor(
    private httpService: HttpService,
    private authService: AuthService
  ) {}

  getAllRentals() {
    // TODO replace http command
    const url = 'rental/list';
    return this.httpService.sendGetRequest(url, {});

    // return Promise.resolve(RENTALS);
  }

  getRentalsPaging(pageno: number) {
    const url = `rental/list`;
    return this.httpService.sendGetRequest(url, {
      pageno
    });
  }

  getRentalsPagingWithOwnerId(pageno: number) {
    const url = 'rental/list/owner/' + this.authService.getLoggedInUser().id;
    const http = this.httpService;
    return new Promise((resolve, reject) => {
      http.sendGetRequest(url, {
        pageno
      }).then((respBody) => {
        resolve(respBody);
      })
        .catch((err) => reject(err));
    });
  }

  getMyRenting(pageno: number) {
    const url = 'rental/list/user/' + this.authService.getLoggedInUser().id;
    const http = this.httpService;
    return new Promise((resolve, reject) => {
      http.sendGetRequest(url, {
        pageno
      }).then((respBody) => {
        resolve(respBody);
      })
        .catch((err) => reject(err));
    });
  }

  searchRentals(term: string) {
    const url = 'rental/search';
    const d = { searchString:  term };
    const http = this.httpService;

    return new Promise((resolve, reject) => {
      http.sendGetRequest(url, d).then((respBody: any) => {
        const rentals = respBody.list;
        resolve(rentals);
      })
        .catch((err) => reject(err));
    });
  }

  sendApprove(rental: Rental) {
    const url = `rental/${rental.id}/approve`;
    return this.httpService.sendPostRequest(url, rental);
  }

  sendFinish(rental: Rental) {
    const url = `rental/${rental.id}/finish`;
    return this.httpService.sendPostRequest(url, rental);
  }

  sendReject(rental: Rental) {
    const reason = 'Rental is rejected as requested.';
    const url = `rental/${rental.id}/reject?rejectDesc=${reason}`;
    return this.httpService.sendPostRequest(url, rental);
  }

  sendRentalRequest(carToRent: Car, rental: Rental) {
    const url = 'rental/create';
    const loggedInUser = this.authService.getLoggedInUser();

    const d = {
      user: loggedInUser,
      car: carToRent,
      price: carToRent.price,
      status: 'PENDING',
      pickUpDate: rental.pickUpDate,
      expectedDate: rental.expectedDate
    };

    return this.httpService.sendPostRequest(url, d);
  }

}
