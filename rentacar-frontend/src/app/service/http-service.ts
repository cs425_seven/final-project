import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {LogService} from './log-service';
import {ResponseDTO} from '../model/response-dto';
import {RtStorageService} from './rt-storage.service';
import {UtilService} from './util-service';


@Injectable()
export class HttpService {

  constructor(
    private http: HttpClient,
    private logger: LogService,
    private util: UtilService,
    private rtStorage: RtStorageService) {
  }

  // baseUrl = 'http://localhost:8080/';
  baseUrl = '';

  static getErrorMessage(resp) {
    const defError = 'An error occurs while sending request.';
    if (!resp || !resp.body || !resp.body.error || !resp.body.error.errorDesc) {
      return defError;
    }

    return resp.body.error.errorDesc;
  }

  sendGetRequest(url, reqParams) {

    const reqUrl = `${this.baseUrl}/${url}`;
    const httpClient = this.http;
    const logger = this.logger;

    let auth = 'Bearer ';
    const token = this.util.getCookie(this.rtStorage.TOKEN_KEY);
    if (token) {
      auth += token.toString();
    } else {
      logger.warn('[http-service][get] no token.');
    }

    const reqHeaders = new HttpHeaders()
      .append('Content-Type', 'application/json; charset=utf-8')
      .append('Accept', 'application/json')
      .append('Authorization', auth);

    return new Promise((resolve, reject) => {

      httpClient.request('GET', reqUrl, {
        headers: reqHeaders,
        params: reqParams,
        observe: 'response'
      })
      .toPromise()
      .then(resp => {
        if (resp && resp.status && resp.status === 200) {
          logger.log('[http-service] http request -> ok');

          const respBody: any = resp.body;
          const dataBody = respBody.body;
          const respData = dataBody.response;
          resolve(respData);
        } else {
          logger.error(resp);
          reject(new Error('An error occurs while sending http request.'));
        }
      })
      .catch((err) => {
        logger.error('[http-service] An error occurs while sending request to ' + url);
        logger.error(err);
        reject(new Error('En unknown error occurs while sending http request.'));
      });
    });

  }

  sendPostRequest(url, postParams) {
    const reqUrl = `${this.baseUrl}/${url}`;

    const httpClient = this.http;
    const logger = this.logger;

    let auth = 'Bearer ';
    const token = this.util.getCookie(this.rtStorage.TOKEN_KEY);
    if (token) {
      auth += token.toString();
    } else {
      logger.warn('[http-service][post] no token.');
    }

    const reqHeaders = new HttpHeaders()
      // .append('Content-Type', 'application/json; charset=utf-8')
      .append('Accept', 'application/json')
      .append('Authorization', auth);

    return new Promise((resolve, reject) => {

      httpClient.post(reqUrl, postParams, { headers: reqHeaders })
      .toPromise()
      .then((resp: ResponseDTO) => {
        if (!resp || !resp.header) {
          this.logger.error('[http-service][send-post] Invalid response: Expect a header in response.');
          return reject(new Error('Invalid response: Expect a header in response.'));
        }
        const header = resp.header;
        if (header.code === 200) {
          logger.log('[http-service][send-post] -> ok');
          resolve(resp.body);
        } else {
          logger.error('[http-service][send-post] -> receive response with not ok status -> ' + JSON.stringify(resp));

          reject(new Error(HttpService.getErrorMessage(resp)));
        }
      })
      .catch((err) => {
        logger.error('[http-service][send-post] An error occurs while sending request to ' + url);
        logger.error(err);
        reject(new Error('En unknown error occurs while sending http request.'));
      });
    });

  }

}
