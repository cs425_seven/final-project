import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RtStorageService {

  TOKEN_KEY = 'car-rental-access-tk';
  LOGGEDIN_USER = 'car-rental-logged-in-user';

  mData: Map<string, object>;

  constructor() {
    this.mData = new Map<string, object>();
  }

  setData(key: string, value: object) {
    this.mData[key] = value;
  }

  getData(key: string): object {
    return this.mData[key];
  }
}
