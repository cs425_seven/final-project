import {Injectable} from '@angular/core';
import {LogService} from './log-service';
import {HttpService} from './http-service';
import {RtStorageService} from './rt-storage.service';
import {Car} from '../model/car';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CarService {
  constructor(
    private logger: LogService,
    private auth: AuthService,
    private http: HttpClient,
    private httpService: HttpService,
    private rtStorage: RtStorageService) {}

  getAllCars() {

    // const url = 'car/list';
    // const http = this.httpService;
    //
    // return new Promise((resolve, reject) => {
    //   http.sendGetRequest(url, {}).then((respBody) => {
    //     // @ts-ignore
    //     const cars = respBody.response;
    //     resolve(cars);
    //   })
    //     .catch((err) => reject(err));
    // });

    return this.getCarsPaging(-1);
  }
  getCarsByOwner() {

    const url = 'car/list/owner';
    const http = this.httpService;
    const params = {
      userId: this.auth.getLoggedInUser().id,
    };
    return new Promise((resolve, reject) => {
      http.sendGetRequest(url, params).then((respBody) => {
        resolve(respBody);
      })
        .catch((err) => reject(err));
    });

  }

  getCarsPaging(pageno: number) {
    const url = `car/list`;
    return this.httpService.sendGetRequest(url, {
      pageno
    });
  }

  searchCars(term: string) {

    const url = 'car/search';
    const d = { searchString:  term };
    const http = this.httpService;

    return new Promise((resolve, reject) => {
      http.sendGetRequest(url, d).then((respBody) => {
        // @ts-ignore
        const cars = respBody.list;
        resolve(cars);
      })
        .catch((err) => reject(err));
    });

    // return new Promise((resolve, reject) => {
    //   setTimeout(resolve.bind(null, CARS), 1000);
    // });
  }

  getCar(carId: number) {
    const url = `car/get/${carId}`;
    const http = this.httpService;

    return new Promise((resolve, reject) => {
      http.sendGetRequest(url, {}).then((respBody) => {
        // @ts-ignore
        const car = respBody.object;
        // const car = respBody.response;
        resolve(car);
      })
        .catch((err) => reject(err));
    });

    // return new Promise((resolve, reject) => {

    //   const car = CARS.find((c: Car) => c.id === carId);
    //   setTimeout(resolve.bind(null, car), 1000);
    // });
  }

  registerCar(car: Car) {
    const url = 'car/register';
    const http = this.httpService;
    car.owner = this.auth.getLoggedInUser();

    //
    // return new Promise((resolve, reject) => {
    //   http.sendPostRequest(url, car).then((respBody) => {
    //     const resp = respBody;
    //     console.log(resp);
    //     resolve(car);
    //   })
    //     .catch((err) => reject(err));
    // });
    return http.sendPostRequest(url, car);
  }

  uploadImages(data: any) {
    const url = 'upload/uploadMultipleFiles';
    return this.httpService.sendPostRequest(url, data);
  }



}
