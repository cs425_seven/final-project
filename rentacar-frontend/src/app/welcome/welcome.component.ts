import { Component, OnInit } from '@angular/core';
import {NotifierService} from '../service/notify.service';
import {LogService} from '../service/log-service';
import {Car} from '../model/car';
import {CarService} from '../service/car-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  cars = [];

  constructor(
    private router: Router,
    private notifier: NotifierService,
    private logger: LogService,
    private carService: CarService
  ) { }

  ngOnInit() {
    this.carService.getAllCars()
    .then((respData: any) => {
      this.cars = respData.list;
    })
    .catch((err) => this.logger.error('Failed to load cars.'));
  }

  onKeyUpOnSearchField(e, value) {
    if (e.key !== 'Enter') {
      return;
    }

    this.carService.searchCars(value)
    .then((cars: any) => {
      this.cars = cars;
      if (!cars || cars.length <= 0) {
        this.notifier.error('No result found.');
      }
      // this.notifier.success('Search done.');
      // this.logger.info(cars);
    })
    .catch((err) => this.logger.error('Failed to search cars.', err));

  }

  onViewDetailSelected($event: Car) {
    this.logger.info($event);
    this.router.navigate([ 'car-detail', { carId: $event.id} ]).then();
  }

}
