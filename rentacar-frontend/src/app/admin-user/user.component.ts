import {Component, Injectable, Injector, OnInit} from '@angular/core';
import { UserService } from '../service/user-service';
import {LogService} from '../service/log-service';
import {User} from '../model/user';
import {NotifierService} from '../service/notify.service';
import {BaseComponent} from '../base/base.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

@Injectable()
// @ts-ignore
export class UserComponent extends BaseComponent {

  pageTile = 'Users Administration';
  users: User[];
  // pageNumber, pageSize
  page: any = {};

  constructor(
    private injector: Injector,
    private userService: UserService,
    private logger: LogService,
    private notifier: NotifierService) {
    super(injector);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {

    super.ngOnInit();

    this.logger.info('User-Component -> initial -> loading users list.');

    // this.userService.getAllUsers()
    this.loadUsers(0);
  }

  loadUsers(pageno: number) {
    this.userService.getUsersPaging(pageno)
      .then((response: object) => {
        // @ts-ignore
        this.users = response.content;
        // @ts-ignore
        this.page = response.pageable;
        // @ts-ignore
        this.page.totalPages = response.totalPages;
      })
      .catch(err => {
        this.logger.error('failed to load users -> ', err);
        this.notifier.error('Cannot load user data. See log for more detail.');
      });
  }

  searchUser(term: string) {
    // this.notifier.error('Feature is being implemented...');
    this.userService.searchUsers(term)
    .then((response: object) => {
      // @ts-ignore
      this.users = response.list;
      this.logger.info('Search result -> ');
      this.logger.info(this.users);
      this.page = null;
    })
    .catch(err => {
      this.logger.error('failed to search users -> ', err);
      this.notifier.error('Cannot search user. See log for more detail.');
    });
  }

  blockUser(user: User) {
    this.userService.blockUser(user)
    .then((isOk: boolean) => {
      this.notifier.success('Blocked user ' + user.userName);
      user.status = 'Blocked';
    })
    .catch((err) => this.notifier.error('Failed to block user (server error).'));

  }

  unBlockUser(user: User) {
    this.userService.unBlockUser(user)
    .then((isOk: boolean) => {
      this.notifier.success('Unblocked user ' + user.userName);
      user.status = 'Active';
    })
    .catch((err) => this.notifier.error('Failed to un-block user (server error).'));

  }

  getUserStatusClass(user: User) {
    const classes = ['alert'];

    classes.push(user.status === 'Active' ? 'alert-success' : 'alert-danger');

    return classes;
  }

  getPageItemClass(pno: number) {
    const classes = [];

    if (pno === this.page.pageNumber) {
      classes.push('active');
    }

    return classes;
  }

  arrayOfPages() {
    return Array.from({ length: this.page.totalPages }, (v, k) => k + 1);
  }

}

class UserComponentImpl extends UserComponent {
}
