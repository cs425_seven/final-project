import { Component, OnInit, Injector } from '@angular/core';
import {Car} from '../model/car';
import { LogService } from '../service/log-service';
import { BaseComponent } from '../base/base.component';
import {RentalService} from '../service/rental-service';
import {Rental} from '../model/rental';
import {RentalStatus} from '../model/constant';
import {NotifierService} from '../service/notify.service';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
// @ts-ignore
export class DashboardComponent  extends BaseComponent {

  myCarRent: Rental[];
  myRenting: Rental[];
  page: any = {};
  constructor(
    private injector: Injector,
    private logger: LogService,
    private authService: AuthService,
    private notifier: NotifierService,
    private rentalService: RentalService) {
      super(injector);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {
    super.ngOnInit();
    this.loadMyRentals(0);
    this.loadMyRenting(0);
  }


  sendApprove(rental: Rental) {
    this.rentalService.sendApprove(rental)
      .then((isOk: boolean) => {
        this.notifier.success('Rent request is approved!');
        rental.status = RentalStatus.Approved;
      })
      .catch((err: object) => this.notifier.error('Failed to approve.'));
  }

  sendReject(rental: Rental) {
    this.rentalService.sendReject(rental)
      .then((isOk: boolean) => {
        this.notifier.success('Rent request is rejected!');
        rental.status = RentalStatus.Declined;
      })
      .catch((err: object) => this.notifier.error('Failed to reject.'));
  }

  sendFinish(rental: Rental) {
    this.rentalService.sendFinish(rental)
      .then((isOk: boolean) => {
        this.notifier.success('Renta is done!');
        rental.status = RentalStatus.Returned;
      })
      .catch((err: object) => this.notifier.error('Failed to finish a rental..'));
  }

  getRentalStatusClass(rental: Rental) {
    const classes = ['alert'];

    switch (rental.status) {
      case RentalStatus.Pending:
        classes.push('alert-warning');
        break;
      case RentalStatus.Approved:
        classes.push('alert-success');
        break;
      case RentalStatus.Declined:
        classes.push('alert-danger');
        break;
      case RentalStatus.Returned:
        classes.push('alert-success');
        break;
      default:
        break;
    }

    return classes;
  }

  getPageItemClass(pno: number) {
    const classes = [];

    if (pno === this.page.pageNumber) {
      classes.push('active');
    }

    return classes;
  }

  arrayOfPages() {
    return Array.from({ length: this.page.totalPages }, (v, k) => k + 1);
  }

  loadMyRentals(pageno: number) {
    this.rentalService.getRentalsPagingWithOwnerId(pageno)
      .then((response: object) => {
        // @ts-ignore
        this.myCarRent = response.content;
        // @ts-ignore
        this.page = response.pageable;
        // @ts-ignore
        this.page.totalPages = response.totalPages;
      })
      .catch(err => {
        this.logger.error('failed to load rental -> ', err);
        this.notifier.error('Cannot load rental data. See log for more detail.');
      });
  }

  loadMyRenting(pageno: number) {
    this.rentalService.getMyRenting(pageno)
      .then((response: object) => {
        // @ts-ignore
        // @ts-ignore
        this.myRenting = response.content;
        // @ts-ignore
        this.page = response.pageable;
        // @ts-ignore
        this.page.totalPages = response.totalPages;

      })
      .catch(err => {
        this.logger.error('failed to load rental -> ', err);
        this.notifier.error('Cannot load rental data. See log for more detail.');
      });
  }

  isCarOwner() {
    const loggedInUser = this.authService.getLoggedInUser();
    if (!loggedInUser) {
      return false;
    }
    return (this.authService.isAdmin(loggedInUser) || this.authService.isCarOwner(loggedInUser));
  }

}
