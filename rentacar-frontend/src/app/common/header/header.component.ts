import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {LogService} from '../../service/log-service';
import {Router} from '@angular/router';
import {NotifierService} from '../../service/notify.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    private logger: LogService,
    private notifier: NotifierService,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  isAuthenticated() {
    const loggedInUser = this.authService.getLoggedInUser();

    return ( loggedInUser !== null && loggedInUser !== undefined);
  }

  isAdminLoggedIn() {
    const loggedInUser = this.authService.getLoggedInUser();
    return this.authService.isAdmin(loggedInUser);
  }

  doLogout() {

    const targetPath = '/';

    this.authService.signOut()
    .then((resp) => {
      this.router.navigate([ targetPath ]);
    })
    .catch((err) => {
      this.router.navigate([ targetPath ]);
    });

  }
  getUserName() {
    return this.authService.getLoggedInUser().userName;
  }
}
