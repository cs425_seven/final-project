import {Component, Injector, OnInit} from '@angular/core';
import {Car} from '../model/car';
import {LogService} from '../service/log-service';
import {CarService} from '../service/car-service';
import {BaseComponent} from '../base/base.component';
import {NotifierService} from '../service/notify.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
// @ts-ignore
export class CarComponent extends BaseComponent {

  pageTile = 'Cars Administration';
  cars: Car[];
  page: any = {};

  constructor(
    private injector: Injector,
    private logger: LogService,
    private notifier: NotifierService,
    private carService: CarService) {

    super(injector);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {

    super.ngOnInit();

    this.logger.info('car-component -> initial -> loading cars list.');

    // this.cars[0].images[0].path

    this.loadCars(0);
    // this.carService.getAllCars()
    // .then((cars: Car[]) => {
    //   this.cars = cars;
    // })
    // .catch(err => {
    //   this.logger.error('failed to load cars -> ', err);
    // });
  }

  loadCars(pageno: number) {
    this.carService.getCarsPaging(pageno)
      .then((response: object) => {
        // @ts-ignore
        this.cars = response.content;
        // @ts-ignore
        this.page = response.pageable;
        // @ts-ignore
        this.page.totalPages = response.totalPages;
      })
      .catch(err => {
        this.logger.error('failed to load users -> ', err);
        this.notifier.error('Cannot load user data. See log for more detail.');
      });
  }

  searchCar(term: string) {
    this.carService.searchCars(term)
    .then((cars: Car[]) => {
      this.cars = cars;
      this.page = null;
    })
    .catch(err => {
      this.logger.error('failed to search cars -> ', err);
      this.notifier.error('Cannot search cars. See log for more detail.');
    });
  }

  getPageItemClass(pno: number) {
    const classes = [];

    if (pno === this.page.pageNumber) {
      classes.push('active');
    }

    return classes;
  }

  arrayOfPages() {
    return Array.from({ length: this.page.totalPages }, (v, k) => k + 1);
  }

}
