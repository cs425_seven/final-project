import { Component, Injector, OnInit} from '@angular/core';
import { LogService } from '../service/log-service';
import { AuthService } from '../service/auth.service';
import { NotifierService } from '../service/notify.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-base',
  template: '',
  styleUrls: []
})
export abstract class BaseComponent implements OnInit {

  protected constructor(private injector: Injector) { }

  pageTile = 'CarRental Administrator';

  static isAuthenticatedPage(path: string) {

    // return false;

    let isAuthPage = true;
    switch (path) {
      case '/':
      case '/login':
      case '/logout':
      case '/register':
      // case '/dashboard':
      case '/test':
        isAuthPage = false;
        break;
    }

    return isAuthPage;
  }

  ngOnInit() {
    const logger: LogService = this.injector.get(LogService);
    const authService: AuthService = this.injector.get(AuthService);
    const notifier: NotifierService = this.injector.get(NotifierService);
    const router: Router = this.injector.get(Router);

    logger.info('onInit -> pageTile = ' + this.pageTile);

    const loggedInUser = authService.getLoggedInUser();
    const path = router.url;
    if (!loggedInUser && BaseComponent.isAuthenticatedPage(path)) {
      notifier.error('Not authenticated access -> BuZZ');
      router.navigate([ 'login' ]).then();
    }

  }

}
