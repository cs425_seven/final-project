import { Component, OnInit } from '@angular/core';
import {User} from '../model/user';
import {LogService} from '../service/log-service';
import {AuthService} from '../service/auth.service';
import {NotifierService} from '../service/notify.service';
import {Router} from '@angular/router';
import {Roles} from '../model/constant';

@Component({
  selector: 'app-signup',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  user: User;

  constructor(
    private logger: LogService,
    private notifier: NotifierService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.user = new User();
    this.user.userName = 'test01';
    this.user.password = '123123123';
    this.user.passwordConfirm = '123123123';
    this.user.firstName = 'user';
    this.user.lastName = 'test';
    this.user.email = 'test01@gmail.com';
    this.user.phone = '64119189118';
  }

  onBtnSubmitClicked(isCarOwner: boolean) {
    if (!this.user.userName || !this.user.password) {
      this.notifier.error('Username or password is empty.');
      return;
    }

    if (this.user.password !== this.user.passwordConfirm) {
      this.notifier.error('Password confirmation doesn\'t match.');
      return;
    }

    this.user.roles.push(isCarOwner ? Roles.CarOwner : Roles.CarRenter);

    this.authService.signup(this.user)
    .then((resp) => {
      this.logger.info('Sign up responded -> ' + JSON.stringify(resp));
      this.notifier.success('Created new user.');
      this.router.navigate([ '/login' ]).then();
    })
    .catch((err) => {
      this.logger.error('Failed to sign up -> ' + err);
      if (err instanceof Error) {
        this.notifier.error(err);
      } else {
        this.notifier.error('Failed to create user. Unknown error.');
      }
    });

    this.logger.info('Going to register new users -> ' + JSON.stringify(this.user));
  }

}
