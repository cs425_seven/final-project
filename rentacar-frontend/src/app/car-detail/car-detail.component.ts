import {Component, Injector, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LogService} from '../service/log-service';
import {CarService} from '../service/car-service';
import {Car} from '../model/car';
import {NotifierService} from '../service/notify.service';
import {RentalService} from '../service/rental-service';
import {Rental} from '../model/rental';
import {BaseComponent} from '../base/base.component';


@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.scss']
})
// @ts-ignore
export class CarDetailComponent extends BaseComponent {

  car: Car = new Car();
  rental: Rental;
  thumbnailImgPath: string;

  constructor(
    private injector: Injector,
    private route: ActivatedRoute,
    private carService: CarService,
    private rentalService: RentalService,
    private logger: LogService,
    private notifier: NotifierService
  ) {

    super(injector);

    this.rental = new Rental();
    this.logger.info('[car-detail] Constructor');
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {

    super.ngOnInit();

    this.logger.info('[car-detail] onInit');
    this.route.params.subscribe(params => {

      this.logger.info('[car-detail] Prepare to load CarInfo');
      // tslint:disable-next-line:radix
      const carId: number = parseInt(params.carId);
      this.loadCarDetail(carId);
    });
  }

  loadCarDetail(carId: number) {

    if (!carId) {
      this.notifier.error('Invalid CarId');
      return;
    }

    this.carService.getCar(carId)
    .then((car: Car) => {
      this.car = car;
      this.selectDefaultThumbnailPath();
      this.notifier.success('Loaded car info.');
    })
    .catch((err) => this.logger.error.bind(null, JSON.stringify(err)));
  }

  sendRentalRequest() {

    if (!this.rental.pickUpDate || !this.rental.expectedDate) {
      this.notifier.error('Please select Pickup and Return dates!');
      return;
    }

    this.rentalService.sendRentalRequest(this.car, this.rental)
    .then((isOk: boolean) => {
      this.notifier.success('Request was sent -> Car: ' + this.car.carBrand);
      // @ts-ignore
      $('#confirmRentReq').modal({ show: true });
    })
    .catch((err) => {
      this.notifier.error('Failed to create rent request. Try again later.');
    });

  }

  hasImages() {
    return this.car && this.car.images && this.car.images.length > 0;
  }

  selectImage(img) {
    this.thumbnailImgPath = img.path;
  }

  selectDefaultThumbnailPath() {
    let imgSrc = '/assets/image/no-car.png';
    if (this.car && this.car.images && this.car.images.length > 0) {
      imgSrc = this.car.images[0].path;
    }

    this.thumbnailImgPath = imgSrc;
  }

}
