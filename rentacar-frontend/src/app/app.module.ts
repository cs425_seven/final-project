import { BrowserModule } from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './admin-user/user.component';
import { CarComponent } from './admin-car/car.component';
import { RentalComponent } from './admin-rental/rental.component';
import { ProfileComponent } from './profile/profile.component';
import { SignInComponent } from './signin/sign-in.component';
import { SignUpComponent } from './signup/sign-up.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CarRegisterComponent } from './car-register/car-register.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { StatisticComponent } from './admin-statistic/statistic.component';
import { LogService } from './service/log-service';
import { HttpService } from './service/http-service';
import { UserService } from './service/user-service';
import { TestComponent } from './test/test.component';
import { CarService } from './service/car-service';
import { NotifierService } from './service/notify.service';
import { CarThumbnailComponent } from './car-thumbnail/car-thumbnail.component';
import { RentalService } from './service/rental-service';
import { EllipsisPipe } from './pipes/ellipsis-pipe';

import { CarDetailComponent } from './car-detail/car-detail.component';
import {UtilService} from './service/util-service';
import {CarStatus} from './pipes/car-status';

export let AppInjector: Injector;

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    CarComponent,
    RentalComponent,
    ProfileComponent,
    SignInComponent,
    SignUpComponent,
    DashboardComponent,
    CarRegisterComponent,
    CarDetailComponent,
    HeaderComponent,
    FooterComponent,
    WelcomeComponent,
    StatisticComponent,
    TestComponent,
    CarThumbnailComponent,
    EllipsisPipe,
    CarStatus,
    CarDetailComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    UtilService,
    LogService,
    NotifierService,
    HttpService,
    UserService,
    CarService,
    RentalService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private injector: Injector) {
    AppInjector = this.injector;
  }
}
