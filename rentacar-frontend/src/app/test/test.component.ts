import { Component, OnInit } from '@angular/core';
import {HttpService} from '../service/http-service';
import {Car} from '../model/car';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  cars = [
    new Car(),
    // new Car('Car 02'),
    // new Car('Car 03'),
    // new Car('Car 04'),
    // new Car('Car 05'),
    // new Car('Car 06'),
  ];

  constructor(
    private httpService: HttpService
  ) { }

  ngOnInit() {
    setTimeout(this.testSendGetReqWithUrlParam.bind(this), 100);
    setTimeout(this.testSendGetReqWithPathVariable.bind(this), 1000);
    setTimeout(this.testPost.bind(this), 2000);
  }

  testPost() {
    const model = {
      name: 'Eugene',
      age: 61
    };

    const url = 'api/test/post-with-model';

    this.httpService.sendPostRequest(url, model)
    .then((resp) => console.log('Send Post with model responded', resp))
    .catch((err) => console.log('Send Post with model Failed', err));
  }

  testSendGetReqWithPathVariable() {
    const url = 'api/test/get-with-path-variable/123';

    this.httpService.sendGetRequest(url, {})
    .then((resp) => {
      console.log('get-with-url-param responded.', resp);
    })
    .catch((err) => {
      console.log('get-with-url-param failed.', err);
    });
  }

  testSendGetReqWithUrlParam() {
    const url = 'api/test/get-with-url-param';
    const params = {
      pName: 'Eugene',
      nb: 61
    };
    this.httpService.sendGetRequest(url, params)
    .then((resp) => {
      console.log('get-with-url-param responded.', resp);
    })
    .catch((err) => {
      console.log('get-with-url-param failed.', err);
    });
  }

}
