import {Pipe, PipeTransform} from '@angular/core';
import {Car} from '../model/car';

@Pipe({
  name: 'carStatus'
})
export class CarStatus implements PipeTransform {

  transform(car: Car) {
    if (!car || !car.status) {
      return 'Unknown';
    }

    let s: string;

    switch (car.status) {
      case '1':
        s = 'Very Good';
        break;
      case '2':
        s = 'Good';
        break;
      case '3':
        s = 'Fair';
        break;
      default:
        s = car.status;
        break;
    }

    return s;
  }
}
