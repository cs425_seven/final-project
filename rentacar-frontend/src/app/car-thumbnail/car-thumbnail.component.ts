import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { Car } from '../model/car';

@Component({
  selector: 'app-car-thumbnail',
  templateUrl: './car-thumbnail.component.html',
  styleUrls: ['./car-thumbnail.component.scss']
})
export class CarThumbnailComponent implements OnInit {

  @Input()
  car: Car;

  @Output()
  viewDetailSelected: EventEmitter<Car> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onCarSelected() {
    this.viewDetailSelected.emit(this.car);
  }

  carThumbnailImg() {
    let imgSrc = '/assets/image/no-car.png';
    if (this.car && this.car.images && this.car.images.length > 0) {
      imgSrc = this.car.images[0].path;
    }

    return imgSrc;
  }

}
