import { Component, Injector } from '@angular/core';
import { Rental } from '../model/rental';
import { LogService } from '../service/log-service';
import { RentalService } from '../service/rental-service';
import { NotifierService } from '../service/notify.service';
import { BaseComponent } from '../base/base.component';
import { RentalStatus } from '../model/constant';
import {Car} from '../model/car';

@Component({
  selector: 'app-rental',
  templateUrl: './rental.component.html',
  styleUrls: ['./rental.component.scss']
})
// @ts-ignore
export class RentalComponent extends BaseComponent {

  rentals: Rental[];
  page: any = {};

  constructor(
    private injector: Injector,
    private rentalService: RentalService,
    private logger: LogService,
    private notifier: NotifierService) {
    super(injector);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {

    super.ngOnInit();

    this.logger.info('rental-component -> initial -> loading rentals list.');

    this.loadRentals(0);

  }

  loadRentals(pageno: number) {
    this.rentalService.getRentalsPaging(pageno)
      .then((response: object) => {
        // @ts-ignore
        this.rentals = response.content;
        // @ts-ignore
        this.page = response.pageable;
        // @ts-ignore
        this.page.totalPages = response.totalPages;
      })
      .catch(err => {
        this.logger.error('failed to load rental -> ', err);
        this.notifier.error('Cannot load rental data. See log for more detail.');
      });
  }

  searchRental(term: string) {
    this.rentalService.searchRentals(term)
      .then((rentals: Rental[]) => {
        this.rentals = rentals;
        this.page = null;

        if (!rentals || rentals.length <= 0) {
          this.notifier.error('No result found.');
        }
      })
      .catch(err => {
        this.logger.error('failed to search rentals -> ', err);
        this.notifier.error('Cannot search rentals. See log for more detail.');
      });

    // this.notifier.error('Function is not implemented yet. SORRY!');
  }

  sendApprove(rental: Rental) {
    this.rentalService.sendApprove(rental)
    .then((isOk: boolean) => {
      this.notifier.success('Rent request is approved!');
      rental.status = RentalStatus.Approved;
    })
    .catch((err: object) => this.notifier.error('Failed to approve.'));
  }

  sendReject(rental: Rental) {
    this.rentalService.sendReject(rental)
    .then((isOk: boolean) => {
      this.notifier.success('Rent request is rejected!');
      rental.status = RentalStatus.Declined;
    })
    .catch((err: object) => this.notifier.error('Failed to reject.'));
  }

  sendFinish(rental: Rental) {
    this.rentalService.sendFinish(rental)
      .then((isOk: boolean) => {
        this.notifier.success('Renta is done!');
        rental.status = RentalStatus.Returned;
      })
      .catch((err: object) => this.notifier.error('Failed to finish a rental..'));
  }

  getRentalStatusClass(rental: Rental) {
    const classes = ['alert'];

    switch (rental.status) {
      case RentalStatus.Pending:
        classes.push('alert-warning');
        break;
      case RentalStatus.Approved:
        classes.push('alert-success');
        break;
      case RentalStatus.Declined:
        classes.push('alert-danger');
        break;
      case RentalStatus.Returned:
        classes.push('alert-success');
        break;
      default:
        break;
    }

    return classes;
  }

  getPageItemClass(pno: number) {
    const classes = [];

    if (pno === this.page.pageNumber) {
      classes.push('active');
    }

    return classes;
  }

  arrayOfPages() {
    return Array.from({ length: this.page.totalPages }, (v, k) => k + 1);
  }

}
